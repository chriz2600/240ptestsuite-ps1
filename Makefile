REGION := $(if $(REGION),$(REGION),jap)
CDLIC_FILE = /usr/local/psxsdk/share/licenses/info$(REGION).dat

all: resources image

prepare:
	@echo "Building for $(REGION)"
	mkdir -p patterns
	mkdir -p binary

toolz: prepare
	gcc -Wall -O3 -I. -o ./tools/lz4compress ./tools/lz4compress.c lz4.c lz4hc.c

resources: toolz
	wav2vag ./resources/beep.wav ./resources/beep.raw -raw
	bmp2tim textures/grid224.bmp patterns/grid224.tim 4 -org=640,0 -noblack -clut=912,480
	bmp2tim textures/grid240.bmp patterns/grid240.tim 4 -org=640,0 -noblack -clut=912,480
	bmp2tim textures/grid256.bmp patterns/grid256.tim 4 -org=640,0 -noblack -clut=912,480
	bmp2tim textures/gridw256224.bmp patterns/gridw256224.tim 4 -org=640,0 -noblack -clut=912,480
	bmp2tim textures/gridw256240.bmp patterns/gridw256240.tim 4 -org=640,0 -noblack -clut=912,480
	bmp2tim textures/gridw256256.bmp patterns/gridw256256.tim 4 -org=640,0 -noblack -clut=912,480
	bmp2tim textures/SMPTE100.bmp patterns/SMPTE100.tim 4 -org=640,0 -clut=912,480
	bmp2tim textures/SMPTE75.bmp patterns/SMPTE75.tim 4 -org=640,0   -clut=912,480
	bmp2tim textures/EBU100.bmp patterns/EBU100.tim 4 -org=640,0 -clut=912,480
	bmp2tim textures/EBU75.bmp patterns/EBU75.tim 4 -org=640,0 -clut=912,480
	bmp2tim textures/colorgray.bmp patterns/colorgray.tim 4 -org=640,0 -clut=912,480
	bmp2tim textures/colorbleed.bmp patterns/colorbleed.tim 4 -org=640,256 -clut=912,480
	bmp2tim textures/colorbleedchk.bmp patterns/colorbleedchk.tim 4 -org=640,0 -clut=912,481
	bmp2tim textures/color.bmp patterns/color.tim 8 -org=640,0 -clut=768,485
	bmp2tim textures/colorgrid.bmp patterns/colorgrid.tim 8 -org=640,0 -clut=768,486
	bmp2tim textures/colorgridw256.bmp patterns/colorgridw256.tim 8 -org=640,0 -clut=768,486
	bmp2tim textures/pluge.bmp patterns/pluge.tim 4 -org=640,0 -clut=912,480
	bmp2tim textures/linearity.bmp patterns/linearity.tim 4 -org=640,0 -noblack -clut=912,480
	bmp2tim textures/linearity224.bmp patterns/linearity224.tim 4 -org=640,0 -noblack -clut=912,480
	bmp2tim textures/linearity240pal.bmp patterns/linearity240pal.tim 4 -org=640,0 -noblack -clut=912,480
	bmp2tim textures/linearityw256224.bmp patterns/linearityw256224.tim 4 -org=640,0 -noblack -clut=912,480
	bmp2tim textures/linearityw256224pal.bmp patterns/linearityw256224pal.tim 4 -org=640,0 -noblack -clut=912,480
	bmp2tim textures/linearitygriddot.bmp patterns/linearitygriddot.tim 4 -org=640,256 -noblack -clut=912,481
	bmp2tim textures/lingrid.bmp patterns/lingrid.tim 4 -org=768,0 -noblack -clut=912,482
	bmp2tim textures/grayramp.bmp patterns/grayramp.tim 8 -org=640,0 -clut=768,480
	bmp2tim textures/sharpness.bmp patterns/sharpness.tim 4 -org=640,0 -clut=912,480
	bmp2tim textures/sharpness224.bmp patterns/sharpness224.tim 4 -org=640,0 -clut=912,480
	bmp2tim textures/sharpnessw256224.bmp patterns/sharpnessw256224.tim 4 -org=640,0 -clut=912,480
	bmp2tim textures/sharpnessw256240.bmp patterns/sharpnessw256240.tim 4 -org=640,0 -clut=912,480
	bmp2tim textures/checkerboard.bmp patterns/checkerboard.tim 4 -org=896,256  -clut=912,480
	bmp2tim textures/circle.bmp patterns/circle.tim 4 -org=320,0  -noblack -clut=912,480
	bmp2tim textures/numbers.bmp patterns/numbers.tim 4 -org=640,0 -mpink -clut=912,484
	#############################
	bmp2tim textures/sonicsky.bmp patterns/sonicsky.tim 4 -org=832,256  -clut=912,499
	bmp2tim textures/sonicback1.bmp patterns/sonicback1.tim 4 -org=640,0  -clut=912,481
	bmp2tim textures/sonicback2.bmp patterns/sonicback2.tim 4 -org=640,128  -clut=912,482
	bmp2tim textures/sonicback3.bmp patterns/sonicback3.tim 4 -org=640,256  -clut=912,483
	bmp2tim textures/sonicback4.bmp patterns/sonicback4.tim 4 -org=640,384  -clut=912,484
	bmp2tim textures/sonicfloor.bmp patterns/sonicfloor.tim 4 -org=768,256 -noblack -clut=912,485
	bmp2tim textures/kiki.bmp patterns/kiki.tim 4 -org=704,0 -clut=912,486
	bmp2tim textures/motoko.bmp patterns/motoko.tim 8 -org=768,0 -clut=768,480
	bmp2tim textures/striped.bmp patterns/striped.tim 4 -org=1016,424 -mpink -clut=912,487
	bmp2tim textures/buzzbomber.bmp patterns/buzzbomber.tim 4 -org=1004,384 -mpink -clut=912,490
	bmp2tim textures/buzzbombershadow.bmp patterns/buzzbombershadow.tim 4 -org=1016,384 -mpink -clut=912,491
	bmp2tim textures/shadow.bmp patterns/shadow.tim 4 -org=1004,424 -mpink -clut=912,492
	bmp2tim textures/convergence.bmp patterns/convergence.tim 4 -org=640,0 -noblack -clut=912,490
	bmp2tim textures/font.bmp patterns/font.tim 4 -mpink -org=960,256  -clut=960,321
	bmp2tim textures/lagper.bmp patterns/lagper.tim 4 -mpink -org=992,424  -clut=912,490
	bmp2tim textures/back.bmp patterns/back.tim 4 -org=944,0  -clut=960,510
	bmp2tim textures/backw256.bmp patterns/backw256.tim 4 -org=960,0  -clut=960,510
	bmp2tim textures/backw384.bmp patterns/backw384.tim 4 -org=928,0  -clut=960,510
	bmp2tim textures/backw512.bmp patterns/backw512.tim 4 -org=896,0  -clut=960,510
	bmp2tim textures/backw640.bmp patterns/backw640.tim 4 -org=864,0  -clut=960,510
	bmp2tim textures/gillian.bmp patterns/gillian.tim 4 -org=1008,256 -mpink -clut=960,511
	./tools/lz4compress ./patterns/grid224.tim ./patterns/grid224.lz4
	./tools/lz4compress ./patterns/grid240.tim ./patterns/grid240.lz4
	./tools/lz4compress ./patterns/grid256.tim ./patterns/grid256.lz4
	./tools/lz4compress ./patterns/gridw256224.tim ./patterns/gridw256224.lz4
	./tools/lz4compress ./patterns/gridw256240.tim ./patterns/gridw256240.lz4
	./tools/lz4compress ./patterns/gridw256256.tim ./patterns/gridw256256.lz4
	./tools/lz4compress ./patterns/SMPTE100.tim ./patterns/SMPTE100.lz4
	./tools/lz4compress ./patterns/SMPTE75.tim ./patterns/SMPTE75.lz4
	./tools/lz4compress ./patterns/EBU100.tim ./patterns/EBU100.lz4
	./tools/lz4compress ./patterns/EBU75.tim ./patterns/EBU75.lz4
	./tools/lz4compress ./patterns/colorgray.tim ./patterns/colorgray.lz4
	./tools/lz4compress ./patterns/colorbleed.tim ./patterns/colorbleed.lz4
	./tools/lz4compress ./patterns/colorbleedchk.tim ./patterns/colorbleedchk.lz4
	./tools/lz4compress ./patterns/color.tim ./patterns/color.lz4
	./tools/lz4compress ./patterns/colorgrid.tim ./patterns/colorgrid.lz4
	./tools/lz4compress ./patterns/colorgridw256.tim ./patterns/colorgridw256.lz4
	./tools/lz4compress ./patterns/pluge.tim ./patterns/pluge.lz4
	./tools/lz4compress ./patterns/linearity.tim ./patterns/linearity.lz4
	./tools/lz4compress ./patterns/linearity224.tim ./patterns/linearity224.lz4
	./tools/lz4compress ./patterns/linearity240pal.tim ./patterns/linearity240pal.lz4
	./tools/lz4compress ./patterns/linearityw256224.tim ./patterns/linearityw256224.lz4
	./tools/lz4compress ./patterns/linearityw256224pal.tim ./patterns/linearityw256224pal.lz4
	./tools/lz4compress ./patterns/linearitygriddot.tim ./patterns/linearitygriddot.lz4
	./tools/lz4compress ./patterns/grayramp.tim ./patterns/grayramp.lz4
	./tools/lz4compress ./patterns/sharpness.tim ./patterns/sharpness.lz4
	./tools/lz4compress ./patterns/sharpness224.tim ./patterns/sharpness224.lz4
	./tools/lz4compress ./patterns/sharpnessw256240.tim ./patterns/sharpnessw256240.lz4
	./tools/lz4compress ./patterns/sharpnessw256224.tim ./patterns/sharpnessw256224.lz4
	./tools/lz4compress ./patterns/lingrid.tim ./patterns/lingrid.lz4
	./tools/lz4compress ./patterns/checkerboard.tim ./patterns/checkerboard.lz4
	./tools/lz4compress ./patterns/sonicsky.tim ./patterns/sonicsky.lz4
	./tools/lz4compress ./patterns/sonicsky.tim ./patterns/sonicsky.lz4
	./tools/lz4compress ./patterns/sonicback1.tim ./patterns/sonicback1.lz4
	./tools/lz4compress ./patterns/sonicback2.tim ./patterns/sonicback2.lz4
	./tools/lz4compress ./patterns/sonicback3.tim ./patterns/sonicback3.lz4
	./tools/lz4compress ./patterns/sonicback4.tim ./patterns/sonicback4.lz4
	./tools/lz4compress ./patterns/sonicfloor.tim ./patterns/sonicfloor.lz4
	./tools/lz4compress ./patterns/kiki.tim ./patterns/kiki.lz4
	./tools/lz4compress ./patterns/motoko.tim ./patterns/motoko.lz4
	./tools/lz4compress ./patterns/striped.tim ./patterns/striped.lz4
	./tools/lz4compress ./patterns/buzzbomber.tim ./patterns/buzzbomber.lz4
	./tools/lz4compress ./patterns/buzzbombershadow.tim ./patterns/buzzbombershadow.lz4
	./tools/lz4compress ./patterns/shadow.tim ./patterns/shadow.lz4
	./tools/lz4compress ./patterns/font.tim ./patterns/font.lz4
	./tools/lz4compress ./patterns/circle.tim ./patterns/circle.lz4
	./tools/lz4compress ./patterns/numbers.tim ./patterns/numbers.lz4
	./tools/lz4compress ./patterns/lagper.tim ./patterns/lagper.lz4
	./tools/lz4compress ./patterns/back.tim ./patterns/back.lz4
	./tools/lz4compress ./patterns/backw256.tim ./patterns/backw256.lz4
	./tools/lz4compress ./patterns/backw384.tim ./patterns/backw384.lz4
	./tools/lz4compress ./patterns/backw512.tim ./patterns/backw512.lz4
	./tools/lz4compress ./patterns/backw640.tim ./patterns/backw640.lz4
	./tools/lz4compress ./patterns/gillian.tim ./patterns/gillian.lz4
	./tools/lz4compress ./patterns/convergence.tim ./patterns/convergence.lz4
	bin2c -n grid224_array -o ./patterns/grid224.c patterns/grid224.lz4 
	bin2c -n grid240_array -o ./patterns/grid240.c patterns/grid240.lz4 
	bin2c -n grid256_array -o ./patterns/grid256.c patterns/grid256.lz4 
	bin2c -n gridw256224_array -o ./patterns/gridw256224.c patterns/gridw256224.lz4 
	bin2c -n gridw256240_array -o ./patterns/gridw256240.c patterns/gridw256240.lz4 
	bin2c -n gridw256256_array -o ./patterns/gridw256256.c patterns/gridw256256.lz4 
	bin2c -n SMPTE100_array -o ./patterns/SMPTE100.c patterns/SMPTE100.lz4 
	bin2c -n SMPTE75_array -o ./patterns/SMPTE75.c patterns/SMPTE75.lz4 
	bin2c -n EBU100_array -o ./patterns/EBU100.c patterns/EBU100.lz4 
	bin2c -n EBU75_array -o ./patterns/EBU75.c patterns/EBU75.lz4 
	bin2c -n colorgray_array -o ./patterns/colorgray.c patterns/colorgray.lz4 
	bin2c -n colorbleed_array -o ./patterns/colorbleed.c patterns/colorbleed.lz4 
	bin2c -n colorbleedchk_array -o ./patterns/colorbleedchk.c patterns/colorbleedchk.lz4 
	bin2c -n color_array -o ./patterns/color.c patterns/color.lz4 
	bin2c -n colorgrid_array -o ./patterns/colorgrid.c patterns/colorgrid.lz4 
	bin2c -n colorgridw256_array -o ./patterns/colorgridw256.c patterns/colorgridw256.lz4 
	bin2c -n pluge_array -o ./patterns/pluge.c patterns/pluge.lz4 
	bin2c -n linearity_array -o ./patterns/linearity.c patterns/linearity.lz4 
	bin2c -n linearity224_array -o ./patterns/linearity224.c patterns/linearity224.lz4 
	bin2c -n linearity240pal_array -o ./patterns/linearity240pal.c patterns/linearity240pal.lz4 
	bin2c -n linearityw256224_array -o ./patterns/linearityw256224.c patterns/linearityw256224.lz4 
	bin2c -n linearityw256224pal_array -o ./patterns/linearityw256224pal.c patterns/linearityw256224pal.lz4 
	bin2c -n linearitygriddot_array -o ./patterns/linearitygriddot.c patterns/linearitygriddot.lz4 
	bin2c -n grayramp_array -o ./patterns/grayramp.c patterns/grayramp.lz4 
	bin2c -n sharpness_array -o ./patterns/sharpness.c patterns/sharpness.lz4 
	bin2c -n sharpness224_array -o ./patterns/sharpness224.c patterns/sharpness224.lz4 
	bin2c -n sharp256240_array -o ./patterns/sharpnessw256240.c patterns/sharpnessw256240.lz4 
	bin2c -n sharp256224_array -o ./patterns/sharpnessw256224.c patterns/sharpnessw256224.lz4 
	bin2c -n lingrid_array -o ./patterns/lingrid.c patterns/lingrid.lz4 
	bin2c -n checkerboard_array -o ./patterns/checkerboard.c patterns/checkerboard.lz4 
	bin2c -n sonicsky_array -o ./patterns/sonicsky.c patterns/sonicsky.lz4 
	bin2c -n sonicback1_array -o ./patterns/sonicback1.c patterns/sonicback1.lz4 
	bin2c -n sonicback2_array -o ./patterns/sonicback2.c patterns/sonicback2.lz4 
	bin2c -n sonicback3_array -o ./patterns/sonicback3.c patterns/sonicback3.lz4 
	bin2c -n sonicback4_array -o ./patterns/sonicback4.c patterns/sonicback4.lz4 
	bin2c -n sonicfloor_array -o ./patterns/sonicfloor.c patterns/sonicfloor.lz4 
	bin2c -n kiki_array -o ./patterns/kiki.c patterns/kiki.lz4 
	bin2c -n motoko_array -o ./patterns/motoko.c patterns/motoko.lz4 
	bin2c -n striped_array -o ./patterns/striped.c patterns/striped.lz4 
	bin2c -n buzzbomber_array -o ./patterns/buzzbomber.c patterns/buzzbomber.lz4 
	bin2c -n buzzbombershadow_array -o ./patterns/buzzbombershadow.c patterns/buzzbombershadow.lz4 
	bin2c -n shadow_array -o ./patterns/shadow.c patterns/shadow.lz4 
	bin2c -n font_array -o ./patterns/font.c patterns/font.lz4 
	bin2c -n circle_array -o ./patterns/circle.c patterns/circle.lz4 
	bin2c -n numbers_array -o ./patterns/numbers.c patterns/numbers.lz4 
	bin2c -n lagper_array -o ./patterns/lagper.c patterns/lagper.lz4 
	bin2c -n back_array -o ./patterns/back.c patterns/back.lz4 
	bin2c -n backw256_array -o ./patterns/backw256.c patterns/backw256.lz4 
	bin2c -n backw384_array -o ./patterns/backw384.c patterns/backw384.lz4 
	bin2c -n backw512_array -o ./patterns/backw512.c patterns/backw512.lz4 
	bin2c -n backw640_array -o ./patterns/backw640.c patterns/backw640.lz4 
	bin2c -n beep_array -o ./resources/beep.h resources/beep.raw 
	bin2c -n gillian_array -o ./patterns/gillian.c patterns/gillian.lz4 
	bin2c -n convergence_array -o ./patterns/convergence.c patterns/convergence.lz4 

image: prepare
	psx-gcc -Wall -O3 -o 240p.elf 240p.c patterns.c tests.c font.c lz4.c textures.c help.c sg_string.c
	elf2exe 240p.elf psx.exe
	rm -f ./binary/240p.bin ./binary/240p.cue
	cd ./binary && mkpsxiso-new ../iso.xml
	rm -f 240p.elf
	rm -f psx.exe

clean:
	rm -f psx.exe 240p.elf ./binary/240p.bin ./binary/240p.cue
	rm -fr patterns resources/beep.h resources/beep.raw
